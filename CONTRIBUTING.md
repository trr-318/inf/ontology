### Contribution to the ontology
Please read the [best practices](https://gitlab.ub.uni-bielefeld.de/trr-318/inf/ontology/-/wikis/Best-Practice-Principles) carefully.

### Workflow

**Discussion**

1. 🐙 Create
   [an issue](https://docs.gitlab.com/ee/user/project/issues/create_issues.html) on
   the Gitlab repository describing the problem and proposed solution.

    Choose the right issue type from among the available choices:

    A) Adding a new entity to the ontology. Ideally, each issue should correspond to only one new term (with only a few subclasses, if needed)
    
    B) Restructuring existing parts of the ontology

    C) Updating definitions of existing entities in the ontology

    D) Other issue



    Discussion about the implementation should take place within the issue. **Important**: Please discuss the proposal within the issue thread **before** starting to work on a solution. For minor changes, which include small changes to improve clarity of definitions and the addition of clarifying annotations, at least one other person from the project team should agree to the proposed change before it is implemented. For major changes, which include adding new entities and restructuring the ontology, at least two other members of the project team need to agree to the change before it is implemented, which should include at least one domain expert and at least one ontology expert. Issues which are contentious, for which it is difficult to reach agreement, should be added to the agenda of the next ontology working group virtual meeting for discussion to reach agreement amongst the full working group. Subsequent to such discussion, the issue's first thread should be updated with a documented record of the conclusions reached.  
